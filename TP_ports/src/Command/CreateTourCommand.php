<?php

namespace App\Command;

use App\Entity\Company;
use App\Entity\Tour;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class TourCreateCommand extends Command
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'tour:create';
    protected static $defaultDescription = 'Creation of a tour';

    protected function configure(): void
    {
        $this
            ->addOption('tourMainEvent', null,InputOption::VALUE_REQUIRED, 'Nom du tour')
            ->addOption('tourCapacity', null, InputOption::VALUE_REQUIRED, '')
            ->addOption('price', null, InputOption::VALUE_REQUIRED, '')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $tourMainEvent = $input->getOption('tourMainEvent');
        $tourCapacity = $input->getOption('tourCapacity');
        $price = $input->getOption('price');

        $tour = new Tour();
        $tour->setMainEvent($tourMainEvent);
        $tour->setCapacity($tourCapacity);
        $tour->setCapacity($tourCapacity);

        $this->entityManager->persist($tour);
        $this->entityManager->flush();



        $io->success('Le tour a été crée');
        return Command::SUCCESS;
    }
}