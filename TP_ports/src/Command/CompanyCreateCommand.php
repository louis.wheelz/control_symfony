<?php

namespace App\Command;

use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class CompanyCreateCommand extends Command
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'company:create';
    protected static $defaultDescription = 'Creation of a company';

    protected function configure(): void
    {
        $this
            ->addOption('compagnyName', null,InputOption::VALUE_REQUIRED, 'Nom de la compagnie')
            ->addOption('compagnyNationality', null, InputOption::VALUE_REQUIRED, 'Nationnalité de la compagnie')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $compagnyName = $input->getOption('compagnyName');
        $compagnyNationality = $input->getOption('compagnyNationality');

        $compagny = new Company();
        $compagny->setName($compagnyName);
        $compagny->setNationality($compagnyNationality);
        $this->entityManager->persist($compagny);
        $this->entityManager->flush();



        $io->success('La compagnie a été crée');
        return Command::SUCCESS;
    }
}