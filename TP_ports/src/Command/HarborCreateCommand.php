<?php

namespace App\Command;

use App\Entity\Harbor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HarborCreateCommand extends Command
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'harbor:create';
    protected static $defaultDescription = 'Creation of an Harbor';

    protected function configure(): void
    {
        $this
            ->addOption('harborName', null, InputOption::VALUE_REQUIRED, 'name of the harbor')
            ->addOption('harborCity', null, InputOption::VALUE_REQUIRED, 'city of the harbor')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $harborName = $input->getOption('harborName');
        $harborCity = $input->getOption('harborCity');

        $io->writeln( 'Le nom du port est : '.$harborName);
        $io->writeln( 'il est dans la ville de : '.$harborCity);

        $harbor = new Harbor();
        $harbor->setName($harborName);
        $harbor->setCity($harborCity);

        $this->entityManager->persist($harbor);
        $this->entityManager->flush();

        # $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
