<?php

namespace App\Command;

use App\Repository\HarborRepository;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HarborListCommand extends Command
{
    private $harborRepository;
    public function __construct(HarborRepository $harborRepository)
    {
        parent::__construct();
        $this->harborRepository = $harborRepository;
    }


    protected static $defaultName = 'harbor:list';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure(): void
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $harborRepository = $this->harborRepository;
        $harbors = $harborRepository->findAll();

        $table = new Table($output);
        $table->setHeaders(['id', 'Name', 'City']);

        foreach ($harbors as $harbor){
            $table->addRow([$harbor->getId(), $harbor->getName(), $harbor->getCity()]);
        }

        $table->render();

        return Command::SUCCESS;
    }
}
