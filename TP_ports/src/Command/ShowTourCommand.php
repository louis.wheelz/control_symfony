<?php

namespace App\Command;

use App\Entity\Company;
use App\Repository\CompanyRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class TourShowCommand extends Command
{
    private $companyRepository;
    public function __construct(CompanyRepository $companyRepository)
    {
        parent::__construct();
        $this->companyRepository = $companyRepository;
    }

    protected static $defaultName = 'tour:show';
    protected static $defaultDescription = 'show of tours';

    protected function configure(): void
    {
        $this
            ->addOption('companyId', null, InputOption::VALUE_REQUIRED, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $companyId = $input->getOption('companyId');
        $company = $this->companyRepository->find($companyId);


        $table = new Table($output);
        $table->setHeaders(['id', 'MainEvent', 'Capacity', 'Price']);
        foreach ($company->getTours() as $tour) {
            $table->addRow([$tour->getId(), $tour->getMainEvent(), $tour->getCapacity(), $tour->getPrice()]);
        }
        $table->render();


        return Command::SUCCESS;
    }
}