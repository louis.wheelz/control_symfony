<?php

namespace App\Command;

use App\Entity\Gate;
use App\Entity\Harbor;
use App\Repository\HarborRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GateCreateCommand extends Command
{
    private $entityManager;
    private $harborRepository;
    public function __construct(EntityManagerInterface $entityManager, HarborRepository $harborRepository)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->harborRepository = $harborRepository;
    }

    protected static $defaultName = 'gate:create';
    protected static $defaultDescription = 'Creation of a gate';

    protected function configure(): void
    {
        $this
            ->addOption('gateNumber', null, InputOption::VALUE_REQUIRED, 'gate number')
            ->addOption('harborId', null, InputOption::VALUE_REQUIRED, 'id of related harbor')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $gateNumber = $input->getOption('gateNumber');
        $harborId = $input->getOption('harborId');
        $harbor = $this->harborRepository->find($harborId);

        if($harbor){
            $io->writeln( 'Le numéro du ponton est : '.$gateNumber);
            $io->writeln( 'le numéro du port est : '.$harborId);

            $gate = new Gate();
            $gate->setNumber($gateNumber);
            $gate->setHarbor($harbor);

            $this->entityManager->persist($gate);
            $this->entityManager->flush();
        } else {
            $io->error("the provided harbor does not exist");
        }



        # $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
