<?php

namespace App\Command;

use App\Repository\HarborRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HarborEditCommand extends Command
{
    private $harborRepository;
    private $entityManager;
    public function __construct(HarborRepository $harborRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->harborRepository = $harborRepository;
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'harbor:edit';
    protected static $defaultDescription = 'Add a short description for your command';

    protected function configure(): void
    {
        $this
            ->addOption('harborId', null, InputOption::VALUE_REQUIRED, 'Harbor ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $harborRepository = $this->harborRepository;
        $entityManager = $this->entityManager;

        $harborId = $input->getOption('harborId');
        $harbor = $harborRepository->find($harborId);


        if($harbor){
            $newName = $io->ask('Nouveau nom: ', $harbor->getName());
            $harbor->setName($newName);

            $newCity = $io->ask('Nouvelle ville: ', $harbor->getCity());
            $harbor->setCity($newCity);
            $entityManager->flush();
        } else {
            $io->error('this harbor does not exist');
        }




        return Command::SUCCESS;
    }
}
