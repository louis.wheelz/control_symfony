<?php

namespace App\Command;



use App\Repository\GateRepository;
use App\Repository\HarborRepository;
use App\Repository\TourRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class TourEditCommand extends Command
{
    private $tourRepository;
    private $entityManager;
    public function __construct(GateRepository $gateRepository, EntityManagerInterface $entityManager, TourRepository $tourRepository)
    {
        parent::__construct();
        $this->tourRepository = $tourRepository;
        $this->entityManager = $entityManager;
    }

    protected static $defaultName = 'tour:edit';
    protected static $defaultDescription = 'edit of tower';

    protected function configure(): void
    {
        $this
            ->addOption('tourId', null, InputOption::VALUE_REQUIRED, 'id du tour')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $tourId = $input->getOption('tourId');
        $tour = $this->tourRepository->find($tourId);

        if ($tour)
        {
            $newName = $io->ask("Nouveau Nom: ", $tour->getMainEvent());
            $tour->setMainEvent($newName);

            $this->entityManager->flush();
        }
        else
        {
            $io->error("Le tour est introuvable");
        }
        $io->success("Nom modifiÃ© !");

        return Command::SUCCESS;
    }
}